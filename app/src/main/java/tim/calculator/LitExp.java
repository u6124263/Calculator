package tim.calculator;
public class LitExp extends Exp {

    double value;
    public LitExp(double v)
    {
        super();
        value=v;
    }
    public String show()
    {
        return ""+value;
    }
    public double evaluate(Subs sub)
    {
        return value;
    }
}
