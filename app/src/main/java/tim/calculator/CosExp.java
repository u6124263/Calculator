package tim.calculator;
import java.lang.*;
public class CosExp extends Exp {
    Exp now;
    public CosExp(Exp now)
    {
        super();
        this.now=now;
    }
    public String show()
    {
        return "cos("+now.show()+")";
    }
    public double evaluate(Subs sub)
    {
        return Math.cos(now.evaluate(sub));
    }
}