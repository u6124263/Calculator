package tim.calculator;
public class MultExp extends Exp {
    Exp left;
    Exp right;
    public MultExp(Exp l, Exp r)
    {
        super();
        left=l;
        right=r;
    }
    public String show()
    {
        return "("+left.show()+"*"+right.show()+")";
    }
    public double evaluate(Subs sub)
    {
        return left.evaluate(sub)*right.evaluate(sub);
    }
}