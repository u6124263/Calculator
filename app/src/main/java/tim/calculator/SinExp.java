package tim.calculator;
import java.lang.*;
public class SinExp extends Exp {
    Exp now;
    public SinExp(Exp now)
    {
        super();
        this.now=now;
    }
    public String show()
    {
        return "sin("+now.show()+")";
    }
    public double evaluate(Subs sub)
    {
        return Math.sin(now.evaluate(sub));
    }
}