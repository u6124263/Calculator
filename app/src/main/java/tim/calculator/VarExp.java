package tim.calculator;
public class VarExp extends Exp {

	String var;
	
	@Override
	public double evaluate(Subs subs) {
		double res = subs.get(var);
		return res;
	}

	@Override
	public String show() {
		return var;
	}

	public VarExp(String var) {
		super();
		this.var = var;
	}

}
