package tim.calculator;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

import static android.icu.lang.UProperty.AGE;

public class Calculator extends AppCompatActivity implements Runnable {
    TextView tv;
    TextView tv2;
    //List<String> history=new ArrayList();
    boolean err=false;
    double input=0;
    double output=0;
    Handler timer;
    String equation="";
    String eq2="(";
    File JSONfile;
    BufferedReader br;
    BufferedWriter bw;
    OutputStream out;
    InputStream in;
    JSONArray jsa;
    Subs subs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        tv=(TextView)findViewById(R.id.textView);
        tv2=(TextView)findViewById(R.id.textView2);
        jsa=new JSONArray();
        subs = new Subs();
        subs.put("x",3);
        JSONfile = new File(getExternalFilesDir(null).getPath(), "equation.json");
        //filePath = Environment.getExternalStorageDirectory().toString()+ "/test.json";
        System.out.println("filePath:" + getExternalFilesDir(null).getPath());

        if(!JSONfile.exists())
        {
            try {
                JSONfile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try
            {
                String s="";
                br=new BufferedReader(new FileReader(JSONfile));
                while (s==br.readLine())
                {
                    JSONObject temp=new JSONObject(s);
                    jsa.put(temp);
                }
                br.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        timer=new Handler();
        //timer.postDelayed(this,2000);
    }

    @Override
    public void run()
    {
        saverealtime();
        timer.postDelayed(this,2000);
    }
    public void saverealtime() //Remove the last equation If it does not contain character "="
    {
        try
        {
            if(jsa.length()>0)
            {
                String equ=jsa.getString(jsa.length()-1);
                if(equ.contains("="))
                {
                    jsa.remove(jsa.length()-1);
                }
            }
            if(equation!="")
            {
                JSONObject temp = new JSONObject();
                temp.put("text", equation);
                jsa.put(temp);
                writejson();
                toastinfor("Change to" + equation);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void writejson()
    {
        try
        {
            bw=new BufferedWriter(new FileWriter(JSONfile));
            bw.write(jsa.toString());
            bw.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public void onClick(View v)
    {

        switch(v.getId())
        {
            case R.id.no1:
                inputnumber(1);
                break;
            case R.id.no2 :
                inputnumber(2);
                break;
            case R.id.no3 :
                inputnumber(3);
                break;
            case R.id.no4 :
                inputnumber(4);
                break;
            case R.id.no5 :
                inputnumber(5);
                break;
            case R.id.no6 :
                inputnumber(6);
                break;
            case R.id.no7 :
                inputnumber(7);
                break;
            case R.id.no8 :
                inputnumber(8);
                break;
            case R.id.no9 :
                inputnumber(9);
                break;
            case R.id.no0 :
                inputnumber(0);
                break;
            case R.id.plus :
                inputmethod('+');
                break;
            case R.id.minus :
                inputmethod('-');
                break;
            case R.id.multi :
                inputmethod('*');
                break;
            case R.id.divide :
                inputmethod('/');
                break;
            case R.id.equal :
                equal1();
                break;
            case R.id.left :
                left();
                break;
            case R.id.right :
                right();
                break;
            case R.id.clear :
                clear();
                tv.setText("");
                tv2.setText("history");
                break;
            case R.id.replay :
                reload();
                break;
            case R.id.x :
                inputx();
                break;
            case R.id.sin :
                inputsin();
                break;
            case R.id.cos :
                inputcos();
                break;
            default:
                break;

        }

    }
    public void inputnumber(int i)
    {
        if(Character.isDigit(eq2.charAt(eq2.length()-1)))
        {
            eq2+=""+i;
        }
        else
        {
            eq2+="#"+i;
        }
        equation+=""+i;
        tv.setText(equation);
    }
    public void inputmethod(char i)
    {
        equation+=""+i;
        eq2+=""+i;
        tv.setText(equation);
    }
//    public void equal()
//    {
//        String []array=equation.split(" ");
//        ArrayList<String> itemList = new ArrayList();
//        itemList.addAll(Arrays.asList(array));
//        try {
//            output = exec(itemList);
//        }
//        catch (Exception e)
//        {
//            warning();
//            return;
//        }
//        if(err)
//        {
//            warning();
//            return;
//        }
//
//        if( output % 1.0 == 0)
//        {
//            long l = (long)output;
//            String result = Long.toString(l);
//            equation+="="+result;
//        }
//        else
//        {
//            equation+="="+output;
//        }
//        tv.setText(equation);
//        try {
//            if(jsa.length()>0)
//            {
//                String equ=jsa.getString(jsa.length()-1);
//                if(equ.contains("="))
//                {
//                    jsa.remove(jsa.length()-1);
//                }
//            }
//            JSONObject temp = new JSONObject();
//            temp.put("text",equation);
//            jsa.put(temp);
//            toastinfor("add" + equation);
//            writejson();
//            clear();
//        }
//        catch (Exception s)
//        {
//            s.printStackTrace();
//        }
//    }
    public void equal1()
    {
        eq2+=")";
        Tokenizer tok = new SimpleTokenizer(eq2);
        try
        {
            Exp exp = Exp.parseExp(tok);
            output = exp.evaluate(subs);
        }
        catch (Exception e)
        {
            warning();
            return;
        }
        if( output % 1.0 == 0)
        {
            long l = (long)output;
            String result = Long.toString(l);
            equation+="="+result;
        }
        else
        {
            equation+="="+output;
        }
        tv.setText(equation);
        try {
            if(jsa.length()>0)
            {
                String equ=jsa.getString(jsa.length()-1);
                if(equ.contains("="))
                {
                    jsa.remove(jsa.length()-1);
                }
            }
            JSONObject temp = new JSONObject();
            temp.put("text",equation);
            jsa.put(temp);
            toastinfor("add" + equation);
            writejson();
            clear();
        }
        catch (Exception s)
        {
            s.printStackTrace();
        }
    }
    public void clear()
    {
        input=0;
        output=0;
        equation="";
        eq2="(";
        err=false;
    }
//    public double exec(ArrayList<String> itemList)
//    {
//
//        if(itemList.size()==3)
//        {
//            return calculate(itemList.get(0),itemList.get(1),itemList.get(2));
//        }
//        int index=find(itemList);
//        if(index>0)
//        {
//            double step=calculate(itemList.get(index-1),itemList.get(index),itemList.get(index+1));
//            itemList.set(index-1,""+step);
//            itemList.remove(index);
//            itemList.remove(index);
//            return exec(itemList);
//        }
//        else
//        {
//            err=true;
//            return 1;
//        }
//    }
//    public int find(ArrayList<String> itemList)
//    {
//        int index=0;
//        int priority=0;
//        for(int i=0;i<itemList.size();i++)
//        {
//            int tempri=0;
//            if(itemList.get(i).charAt(0)=='*'||itemList.get(i).charAt(0)=='/')
//            {
//                tempri=100-i;
//            }
//            else if(itemList.get(i).charAt(0)=='+'||itemList.get(i).charAt(0)=='-')
//            {
//                tempri=20-i;
//            }
//            if(tempri>priority)
//            {
//                index=i;
//                priority=tempri;
//            }
//        }
//        return index;
//    }
//    public double calculate(String X,String method,String Y)
//    {
//        double x=Double.parseDouble(X);
//        double y=Double.parseDouble(Y);
//        switch(method.charAt(0))
//        {
//            case '+':
//                return x+y;
//            case '-':
//                return x-y;
//            case '*':
//                return x*y;
//            case '/':
//                if(y==0)
//                {
//                    err=true;
//                    return 1;
//                }
//                return x/y;
//            default:
//                err=true;
//        }
//        return 1;
//    }
    public void warning()
    {
        tv.setText("error");
        Toast.makeText(getApplicationContext(), "Error! Invalid equation", Toast.LENGTH_SHORT).show();
        clear();
    }
    public void reload()
    {

        String jsoneq="";
        try
        {
            jsoneq=jsa.getJSONObject(jsa.length()-1).getString("text");
            tv2.setText(jsoneq);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    public void toastinfor(String message)
    {
        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
    }
    public void inputx()
    {
        equation=equation+"x";
        eq2=eq2+"$x";
        tv.setText(equation);
    }
    public void left()
    {
        equation=equation+"(";
        eq2=eq2+"(";
        tv.setText(equation);
    }
    public void right()
    {
        equation=equation+")";
        eq2=eq2+")";
        tv.setText(equation);
    }
    public void inputsin()
    {
        equation=equation+"sin(";
        eq2=eq2+"@sin(";
        tv.setText(equation);
    }
    public void inputcos()
    {
        equation=equation+"cos(";
        eq2=eq2+"@cos(";
        tv.setText(equation);
    }

}
