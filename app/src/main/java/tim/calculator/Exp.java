package tim.calculator;
import java.util.ArrayList;
public abstract class Exp {
	public abstract double evaluate(Subs subs);

	public abstract String show();

	static public Exp parseExp(Tokenizer tok) {
		System.out.println("current: "+tok.current().toString());
		if (tok.current().equals("#"))
		{
			tok.next();
			double a=new Double(tok.current().toString());
			tok.next();
			return new LitExp(a);
		}
		if (tok.current().equals("$"))
		{
			tok.next();
			String a=tok.current().toString();
			tok.next();
			return new VarExp(a);
		}
		if(tok.current().equals("("))
		{
			tok.next();
			System.out.println("ls");
			Exp left=parseExp(tok);
			char operator=tok.current().toString().charAt(0);
			System.out.println(""+operator);
			tok.next();
			if(operator==')')
			{
				return left;
			}
			Exp right=parseExp(tok);
			tok.parse(")");
			switch (operator) {
				case '+':
					return new AddExp(left,right);
				case '-':
					return new MinusExp(left,right);
				case '*':
					return new MultExp(left,right);
				case '/':
					return new DivideExp(left,right);
				default:
				throw new Error();
			}
		}
		if(tok.current().equals("@"))
		{
			tok.next();
			if(tok.current().equals("sin"))
			{
				tok.next();
				Exp now=parseExp(tok);
				return new SinExp(now);
			}
			if(tok.current().equals("cos"))
			{
				tok.next();
				Exp now=parseExp(tok);
				return new CosExp(now);
			}
		}
		throw new Error();
	}
	
}
